#include <iostream> 
#include "caneta.hpp"

using namespace std;

int main(int argc, char ** argv) {
   
   Caneta caneta1;
   Caneta caneta2;
   Caneta * caneta3;
   cout << "O ponteiro da caneta3 criado" << endl;
   caneta3 = new Caneta();

   Caneta * caneta4 = new Caneta();
   
   cout << "Cor da caneta1: " << caneta1.getCor() << endl;
   cout << "Marca da caneta1: " << caneta1.getMarca() << endl;
   cout << "Preço da caneta1: " << caneta1.getPreco() << endl;

   caneta1.setNome("Caneta1");
   caneta1.setCor("Azul");
   caneta1.setMarca("Pilot");
   caneta1.setPreco(8.0);

   caneta2.setNome("Caneta2");
   caneta2.setCor("Vermelha");
   caneta2.setMarca("Bic");
   caneta2.setPreco(7.5);

   caneta3->setNome("Caneta3");
   caneta3->setCor("Preta");
   caneta3->setMarca("Faber Castel");
   caneta3->setPreco(9.0);

   caneta4->setNome("Caneta4");
   caneta4->setCor("Verde");
   caneta4->setMarca("Compactor");
   caneta4->setPreco(6.0);


   cout << "Cor da caneta1: " << caneta1.getCor() << endl;
   cout << "Marca da caneta1: " << caneta1.getMarca() << endl;
   cout << "Preço da caneta1: " << caneta1.getPreco() << endl;

   cout << "Cor da caneta2: " << caneta2.getCor() << endl;
   cout << "Marca da caneta2: " << caneta2.getMarca() << endl;
   cout << "Preço da caneta2: " << caneta2.getPreco() << endl;

   cout << "Cor da caneta3: " << caneta3->getCor() << endl;
   cout << "Marca da caneta3: " << caneta3->getMarca() << endl;
   cout << "Preço da caneta3: " << caneta3->getPreco() << endl;
   
   cout << "Cor da caneta4: " << caneta4->getCor() << endl;
   cout << "Marca da caneta4: " << caneta4->getMarca() << endl;
   cout << "Preço da caneta4: " << caneta4->getPreco() << endl;
   
   delete(caneta3);
   delete(caneta4);

   return 0;	
}

